FROM maven:3.6.3-openjdk-11 AS MAVEN_BUILD

ARG CONFIG_USERNAME_ARG
ARG CONFIG_PASSWORD_ARG
ARG CONFIG_SERVER_URL_ARG

ENV CONFIG_USERNAME=$CONFIG_USERNAME_ARG
ENV CONFIG_PASSWORD=$CONFIG_PASSWORD_ARG
ENV CONFIG_SERVER_URL=$CONFIG_SERVER_URL_ARG

COPY pom.xml /build/
COPY src /build/src/
WORKDIR /build/
RUN mvn package -DargLine="-Dspring.profiles.active=prod"


FROM openjdk:8-jre

WORKDIR /app
COPY --from=MAVEN_BUILD /build/target/*.jar /app/registry-server.jar

ENTRYPOINT ["java", "-jar", "-Xmx300m", "-Xss512k", "-Dspring.profiles.active=prod", "registry-server.jar"]
